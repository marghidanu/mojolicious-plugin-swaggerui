# mojolicious-plugin-swaggerui
Swagger UI plugin for Mojolicious

## Build status

[![pipeline status](https://gitlab.com/marghidanu/mojolicious-plugin-swaggerui/badges/master/pipeline.svg)](https://gitlab.com/marghidanu/mojolicious-plugin-swaggerui/commits/master)

## Synopsis
```perl
    # Mojolicious Lite
    plugin 'SwaggerUI' => {
        route => app->routes()->any('/swagger'),
        url => '/swagger.json',
    };
    
    # Mojolicious Full
    $app->plugin(
        SwaggerUI => {
            route   => $app->routes()->any('api'),
            url     => "/api/v1",
            title   => "Mojolicious App",
            favicon => "/images/favicon.png"
        }
    );
```
## Description
The plugin allows you to run the Swagger UI component inside your Mojolicious application.

## Option
### route
```perl
    plugin 'SwaggerUI' => { 
        route => app()->routes()->any('/swagger') 
    };
```
Route for the swagger-ui component. It defaults to a any route on /swagger-ui

### url
```perl
    plugin 'SwaggerUI' => {
        url => '/swagger.json'
    };
```
Url for the JSON Swagger specification. It defaults to /v1

**NOTE:** Mojolicious::Plugin::OpenAPI can expose the JSON Swagger spec under the base path route. You can just point the path in her and it will automatically work.

### title
```perl
    plugin 'SwaggerUI' => {
        title => 'Project Title'
    };
```
The HTML title that you want to show on swagger-ui page. Deafult to 'Swagger UI'

### favicon
```perl
    plugin 'SwaggerUI' => {
        favicon => '/images/favicon.png'
    };
```
Favicon which you want to associate with swagger-ui page.

It will be served automatically from a 'public' directory if it exists.
In case of non existence mojolicious default favicon will be displayed.

## Development environment

    docker-compose build
    docker-compose run --rm app prove -b